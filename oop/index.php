<?php
    // release 0 //////////////////////////////
    require_once ("animal.php");
    require_once ("frog.php");
    require_once ("ape.php");

    $sheep = new Animal("shaun");

    echo "Nama hewan : ".$sheep->name."<br>"; // "shaun"
    echo "Legs : ".$sheep->legs."<br>"; // 4
    echo "Cold blooded ? : ".$sheep->cold_blooded."<br><br>"; // "no"
    // /////////////////////////////////////////





    // release 1 ////////////////////////////// 
    $kodok = new Frog("buduk");
    echo "Nama hewan : ".$kodok->name."<br>"; 
    echo "Legs : ".$kodok->legs."<br>"; 
    echo "Cold blooded ? : ".$kodok->cold_blooded."<br>"; 
    echo "Jump : ".$kodok->jump()."<br><br>"; // "hop hop"

    $sungokong = new Ape("kera sakti");
    echo "Nama hewan : ".$sungokong->name."<br>"; 
    echo "Legs : ".$sungokong->legs."<br>"; 
    echo "Cold blooded ? : ".$sungokong->cold_blooded."<br>"; 
    echo "Yell : ".$sungokong->yell()."<br><br>"; // "Auooo"

    
    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>