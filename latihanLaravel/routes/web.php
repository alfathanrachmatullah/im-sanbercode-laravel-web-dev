<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('Home');
// });

Route::get('/', [HomeController::class, "Home"]);
Route::get('/register', [AuthController::class, "Register"]);
Route::post('/welcome', [AuthController::class, "Welcome"]);

Route::get('/data-tables', function () {return view('data-tables');});
Route::get('/table', function () {return view('tables');});


Route::get('/master', function () {return view('layouts.master');});


Route::get('/cast', [CastController::class, "index"]);
Route::get('/cast/create', [CastController::class, "create"]);
Route::post('/cast', [CastController::class, "store"]);
Route::get('/cast/{cast_id}', [CastController::class, "show"]);
Route::get('/cast/{cast_id}/edit', [CastController::class, "edit"]);
Route::put('/cast/{cast_id}', [CastController::class, "update"]);
Route::delete('/cast/{cast_id}', [CastController::class, "destroy"]);



// Route::get('/', function () {return view('welcome');});
