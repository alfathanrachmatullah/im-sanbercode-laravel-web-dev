@extends('layouts.master')
@section('judul','Detail Cast')
@section('content')
    <h1>{{ $cast->nama }}</h1>
    <h4>Benefit Join di SanberBook : {{ $cast->umur }} tahun</h4>
    <h4>Moto Hidup : </h4>
    <p>{{ $cast->bio }}</p>
@endsection