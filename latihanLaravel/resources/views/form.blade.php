@extends('layouts.master')
@section('judul','Form')
@section('content')
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name=fName><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name=lName><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender"> Male <br> <!-- name="" agar yang dipilih hanya 1 di name yang sama -->
        <input type="radio" name="gender"> Female <br> <!-- name="" sejenis cvariable yang nyimpen value dari formnya -->
        <input type="radio" name="gender"> Other <br><br>
        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="">Indonesian</option>
            <option value="">American</option>
            <option value="">Japan</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="langSpoken"> Bahasa Indonesia <br>
        <input type="checkbox" name="langSpoken"> English <br>
        <input type="checkbox" name="langSpoken"> Other <br><br>
        <label>Bio:</label><br><br>
        <textarea cols="33" rows="10"></textarea><br>


        <!-- <input type="submit" value="Sign Up"> -->
        <!-- atau -->
        <button type="submit">Sign Up</button>
    </form>
@endsection