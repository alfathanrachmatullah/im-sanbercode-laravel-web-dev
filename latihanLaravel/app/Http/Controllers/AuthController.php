<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Register(){
        return view("form");
    }

    public function Welcome(Request $request){
        $firstName = $request['fName'];
        $lastName = $request['lName'];
        
        return view('welcome',[
            "namaDepan" => $firstName
            ,"namaBelakang" => $lastName
        ]
    );
    }
}
