<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index(){
        $cast = DB::table('cast')->get();
        return view("castIndex", [
            "cast" => $cast
        ]);
    }

    public function create(){
        return view("castCreate");
    }

    public function store(Request $request){
        $request->validate([
            'castName'=>'required'
            ,'castUmur'=>'required'
            ,'castBio'=>'required'
        ]);
        DB::table('cast')->insert([
            'nama'=>$request["castName"]
            ,'umur'=>$request["castUmur"]
            ,'bio'=>$request["castBio"]
        ]);
        return redirect('/cast');
    }

    public function show($id){
        $castByID = DB::table('cast')->find($id);
        return view("castDetail", [
            "cast" => $castByID
        ]);
    }

    public function edit($id){
        $castByID = DB::table('cast')->find($id);
        return view("castEdit", [
            "cast" => $castByID
        ]);
    }
    
    public function update($id, Request $request){
        $request->validate([
            'castName'=>'required'
            ,'castUmur'=>'required'
            ,'castBio'=>'required'
        ]);

        DB::table('cast')->where('id', $id)
        ->update([
            'nama'=>$request["castName"]
            ,'umur'=>$request["castUmur"]
            ,'bio'=>$request["castBio"]
        ]);
        return redirect('/cast');
    }
    
    public function destroy($id){
        $del = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}